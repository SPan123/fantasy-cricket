import { Routes } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { PlayerSelctionComponent } from "./player-selction/player-selction.component";
import { MatchDetailsComponent } from "./match-details/match-details.component";
import { MatchSquadComponent } from "./match-squad/match-squad.component";
import { LoginComponent } from "./login/login.component";
import { UserLandingComponent } from "./user-landing/user-landing.component";
import { PlayerInfoComponent } from "./player-info/player-info.component";
import { DashboardComponent } from "./dashboard/dashboard.component";


export const HTML_ROUTES: Routes = [
    {
        path: '',
        component: HomeComponent
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'user-landing',
        component: UserLandingComponent
    },
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'player-selection',
        component: PlayerSelctionComponent
    },
    {
        path: 'match-details',
        component: MatchDetailsComponent
    },
    {
        path: 'match-squad/:uId',
        component: MatchSquadComponent
    },
    {
        path: 'player-info/:pId',
        component: PlayerInfoComponent
    },
    {
        path: 'player-selction',
        component: PlayerSelctionComponent
    },
    {
        path: 'dashboard/:matchId',
        component: DashboardComponent
    }
];