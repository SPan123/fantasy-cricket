import { Component, OnInit } from '@angular/core';
import { MatchInfoService } from '../services/match-info/match-info.service';
import { Router } from '@angular/router';

@Component({
	selector: 'fc-match-details',
	templateUrl: './match-details.component.html',
	styleUrls: ['./match-details.component.scss']
})
export class MatchDetailsComponent implements OnInit {

	public uId: number;
	public score: any;
	public finalScore: string;
	public id: number = 0;

	constructor(
		private matchInfoService: MatchInfoService,
		private route: Router
	) { }

	ngOnInit() {

		this.matchInfoService.uniqueIdSource.subscribe((uId) => this.uId = uId);

		this.matchInfoService.getScores(this.uId).subscribe((score) => {
			this.score = score.data;
			// let activeTab = document.querySelector('.nav-link');
			// if (activeTab) {
			// 	activeTab.classList.add('active');
			// }
		});
		this.matchInfoService.getFinalScore(this.uId).subscribe((data) => {
			this.finalScore = data.score;
		});
	}

	public showInnings(id: number): void {
		let removeActive = (val) => {
			val.classList.remove('active');
		}
		let el = <HTMLElement>event.currentTarget;
		if (el) {
			let navLinks = [].slice.call(el.parentElement.parentElement.querySelectorAll('.nav-link.active'));
			navLinks.every(removeActive);
			el.classList.add('active');
		}
		this.id = id;
	}

	public showPlayerDetails(pId: number) {
		this.route.navigate(['player-info', pId]);
	}

}
