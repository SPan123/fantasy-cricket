import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PlayerStatsService } from '../services/player-stats/player-stats.service';

@Component({
  selector: 'fc-player-info',
  templateUrl: './player-info.component.html',
  styleUrls: ['./player-info.component.scss']
})
export class PlayerInfoComponent implements OnInit {

  private playerId: number;
  public player: any;

  constructor(private route: Router,
    private activatedRoute: ActivatedRoute,
    private playerStatsService: PlayerStatsService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((param) => {
      this.playerId = +param['pId'];
    });
    this.playerStatsService.getPlayerStats(this.playerId).subscribe((res) => this.player = res);
  }

}
