/**
 * Library Modules
 */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HTML_ROUTES } from "./app.routes";

/**
 * Services
 */
import { MatchInfoService } from './services/match-info/match-info.service';
import { MatchSquadService } from './services/match-squad/match-squad.service';
import { UserDetailsService } from './services/user-details/user-details.service';
import { PlayerStatsService } from './services/player-stats/player-stats.service';
import { PlayerDetailsService } from './services/player-details/player-details.service';
import { SquadSelctionService } from './services/squad-selction/squad-selction.service';

/**
 * Components
 */
import { AppComponent } from './app.component';

import { HomeComponent } from './home/home.component';
import { PlayerSelctionComponent } from './player-selction/player-selction.component';
import { CarouselComponentComponent } from './carousel-component/carousel-component.component';
import { ScrollerComponent } from './scroller/scroller.component';
import { MatchDetailsComponent } from './match-details/match-details.component';
import { MatchSquadComponent } from './match-squad/match-squad.component';
import { LoginComponent } from './login/login.component';
import { User } from './model/user';
import { PlayerList } from './model/player-list';
import { UserLandingComponent } from './user-landing/user-landing.component';
import { PlayerInfoComponent } from './player-info/player-info.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DataServiceService } from './services/data-service/data-service.service';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        PlayerSelctionComponent,
        CarouselComponentComponent,
        ScrollerComponent,
        MatchDetailsComponent,
        MatchSquadComponent,
        LoginComponent,
        UserLandingComponent,
        PlayerInfoComponent,
        DashboardComponent
    ],
    exports: [
        RouterModule
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot(HTML_ROUTES, {
            useHash: true
        })
    ],
    providers: [
        MatchInfoService,
        MatchSquadService,
        UserDetailsService,
        User,
        PlayerList,
        SquadSelctionService,
        PlayerDetailsService,
        PlayerStatsService,
        DataServiceService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
