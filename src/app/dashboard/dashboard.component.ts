import { Component, OnInit } from '@angular/core';
import { UserDetailsService } from '../services/user-details/user-details.service';
import { ActivatedRoute } from '@angular/router';
import { DataServiceService } from '../services/data-service/data-service.service';
import { MatchInfoService } from '../services/match-info/match-info.service';

@Component({
	selector: 'fc-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

	public userList: any = [];
	public fantasyScores: any = [];
	public players: any = [];
	public userName: string;
	public winner: string;
	private matchId: number;

	constructor(
		private userDetailsService: UserDetailsService,
		private matchInfoService: MatchInfoService,
		private dataService: DataServiceService,
		private activatedRoute: ActivatedRoute
	) { }

	ngOnInit() {
		this.activatedRoute.params.subscribe((param) => {
			this.matchId = +param['matchId'];
		});
		this.userDetailsService.getUserScores(this.matchId).subscribe((data) => {
			this.userList = data;
			this.matchInfoService.getScores(this.matchId).subscribe(matchData => {
				this.userList.forEach((score) => {
					this.dataService.getMatchDetails(score.id, matchData, (JSON.parse(score.matchInfo)).players);
				});
			});
		});
	}

	public getSquadList(uId: number, uName: string) {
		this.userName = uName;
		this.userList.forEach((elm) => {
			if(elm.id === uId) {
				this.players = (JSON.parse(elm.matchInfo)).players;
			}
		});
	}

}
