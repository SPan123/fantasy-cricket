import { Component, OnInit, HostListener, ChangeDetectionStrategy, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatchInfoService } from '../services/match-info/match-info.service';
import { PlayerDetailsService } from '../services/player-details/player-details.service';
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { UserDetailsService } from '../services/user-details/user-details.service';

@Component({
	selector: 'fc-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	public pageAtBottom: boolean;
	public matches: any;
	public title: string = 'Upcoming';
	public playerData: any;
	public playerName: string;
	public userId: number;
	public userName: string;
	public noPlayer: boolean;

	private subject: Subject<string> = new Subject();

	@ViewChild('headerContent') headerContent: ElementRef;

	constructor(
		private route: Router,
		private matchInfoService: MatchInfoService,
		private playerDetailsService: PlayerDetailsService,
		private userDetailsService: UserDetailsService
	) { }

	public ngOnInit(): void {
		this.scrollPromptStatus();
		this.getAll('matches');
		this.subject.pipe(debounceTime(500)).subscribe(res => {
			this.playerDetailsService.findPlayerDetails(res).subscribe(data => this.playerData = data.data);
		});
		this.userId = localStorage.userId;
		if (this.userId) this.getUserName(this.userId);
	}

	private getUserName(uId): void {
		this.userDetailsService.getUserDetails(uId).subscribe((data) => {
			this.userName = data[0].firstName;
		});
	}

	public toggleHeader(): void {
		if(this.headerContent.nativeElement.classList.contains('show')) {
			this.headerContent.nativeElement.classList.remove('show');
		}
		else {
			this.headerContent.nativeElement.classList.add('show');
		}
	}


	public setTitle(gameTitle: string) {
		this.title = gameTitle;
	}

	public findPlayer(name): void {
		if(name) {
			this.noPlayer = false;
			this.subject.next(name);
		}
		else {
			this.noPlayer = true;
		}
	}

	public playerInfo(pId): void {
		this.route.navigate(['player-info', pId]);
	}

	public getAll(str: string): void {
		this.matchInfoService.getAll(str).subscribe(matches => {
			if (str === 'cricket') {
				this.matches = matches.data;
			}
			else {
				this.matches = matches.matches;
			}
			this.scrollPromptStatus();
		});
	}

	private scrollPromptStatus(): void {
		let pageHeight: number = document.documentElement.scrollHeight;
		let scrolledPage: number = window.pageYOffset;
		let windowHeight: number = window.innerHeight;
		const footerHeight: number = 0;
		if (Math.ceil(scrolledPage) < Math.ceil(pageHeight - windowHeight - footerHeight)) {
			this.pageAtBottom = false;
		} else {
			this.pageAtBottom = true;
		}
	}

	public selectPlayers(): void {
		this.route.navigate(['/player-selection']);
	}

	public userLogout(): void {
		if (this.userId) {
			localStorage.removeItem('userId');
			this.userName = undefined;
			this.userId = undefined;
			this.route.navigate(['/home']);
		}
		else this.route.navigate(['/login']);
	}

	@HostListener('window:scroll', [])
	public onWindowScroll(): void {
		this.scrollPromptStatus();
	}

	public ngOnDestroy(): void {
	}


}
