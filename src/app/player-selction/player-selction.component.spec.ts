import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerSelctionComponent } from './player-selction.component';

describe('PlayerSelctionComponent', () => {
  let component: PlayerSelctionComponent;
  let fixture: ComponentFixture<PlayerSelctionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerSelctionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerSelctionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
