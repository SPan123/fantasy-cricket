import { Component, OnInit } from '@angular/core';
import { UserDetailsService } from '../services/user-details/user-details.service';
import { Router } from '@angular/router';
import { MatchSquadService } from '../services/match-squad/match-squad.service';
import { MatchInfoService } from '../services/match-info/match-info.service';
import { DataServiceService } from '../services/data-service/data-service.service';

@Component({
	selector: 'fc-player-selction',
	templateUrl: './player-selction.component.html',
	styleUrls: ['./player-selction.component.scss']
})
export class PlayerSelctionComponent implements OnInit {

	public userId;
	public players;
	public uniqueId: number;
	public matchStarted: boolean;
	public showMatchInfo: boolean;
	public matchTitle: string;
	public battingMsg: string = 'isn\'t in action yet!!';
	public playerRecord: any[] = [];
	public playerTotal: number = 0;
	public extraPlayers:any = [];
	public playerStatus: string = 'is resting!!';
	public userScore: number = 0;

	constructor(
		private userDetailsService: UserDetailsService,
		private route: Router,
		private matchSquadService: MatchSquadService,
		private matchInfoService: MatchInfoService,
		private dataService: DataServiceService
	) { }

	ngOnInit() {
		this.userId = localStorage.userId;
		this.userDetailsService.getUserDetails(this.userId).subscribe(data => {
			data = JSON.parse(data[0].matchInfo);
			this.players = data.players;
			this.uniqueId = data.matchId;
			this.matchInfoService.getScores(this.uniqueId).subscribe(data => {
				this.matchStarted = data.data.matchStarted;
				this.dataService.getMatchDetails(this.userId, data, this.players);
			});
		});
	}

	

	public getMatchInfo(): void {
		if(!this.showMatchInfo) {
			this.matchInfoService.getId(this.uniqueId);
			this.showMatchInfo = true;	
		}
		else {
			this.showMatchInfo = false;
		}
	}

	public editPlayers(): void {
		this.route.navigate(['match-squad', this.userId]);
		this.matchSquadService.getMatchData(this.uniqueId);
	}

	public goToDashboard() {
		this.route.navigate(['dashboard', this.uniqueId]);
	}

}
