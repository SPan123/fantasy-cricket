import { Injectable } from '@angular/core';
import { MatchInfoService } from '../match-info/match-info.service';
import { SquadSelctionService } from '../squad-selction/squad-selction.service';

@Injectable()
export class DataServiceService {

	public userId;
	public players;
	public matchStarted: boolean;
	public showMatchInfo: boolean;
	public matchTitle: string;
	public batting: any[] = [];
	public bowling: any[] = [];
	public fielding: any[] = [];
	public playerRecord: any[] = [];
	public playerTotal: number = 0;
	public playingXI: any = [];
	public extraPlayers:any = [];
	public totalPlayers: any = [];
	public userScore: number = 0;

	constructor(
		private matchInfoService: MatchInfoService,
		private squadSelectionService: SquadSelctionService
		) { }

	public getMatchDetails(userId, data, players?): void {
		this.userId = userId;
		this.extraPlayers.length = 0;
		this.totalPlayers.length = 0;
		this.playerRecord.length = 0;
		this.matchTitle = `${data.data.team[0].name} vs ${data.data.team[1].name}`;
		this.matchStarted = data.data.matchStarted;
		this.batting = data.data.batting;
		this.bowling = data.data.bowling;
		this.fielding = data.data.fielding;
		this.playingXI = data.data.team;
		this.extraPlayers.push(...players);
		this.playingXI.forEach((team) => {
			team.players.forEach((currentPlayer) => {
				players.forEach((selectedPlayer) => {
					if (selectedPlayer.playerId === +currentPlayer.pid) {
						this.totalPlayers.push(currentPlayer);
						this.extraPlayers.splice(this.extraPlayers.indexOf(selectedPlayer), 1);
					}
				});
			});
		});
		this.getBattingRecord(this.batting);
	}

	private getBattingRecord(runs): void {
		runs.forEach((val) => {
			val.scores.forEach((pId) => {
				this.totalPlayers.forEach((player) => {
					if (+player.pid === +pId.pid) {
						pId.playerTotal = (pId.R + (pId["4s"] * 5) + (pId["6s"] * 10));
						this.playerRecord.push(pId);
						this.totalPlayers.splice(this.totalPlayers.indexOf(player), 1);
					}
				});
			});
		});
		this.playerRecord.push(...this.totalPlayers);
		this.getBowlingRecord(this.bowling);
	}

	/**
	 * 
	 * @param wickets 
	 * remove when api return accurate fielding records which also comes with wickets bowler has taken
	 */
	private getBowlingRecord(wickets): void {
		wickets.forEach((val) => {
			val.scores.forEach((pId) => {
				this.playerRecord.forEach((player) => {
					if (+player.pid === +pId.pid) {
						if (!player.playerTotal) {
							player.playerTotal = this.playerTotal;
						}
						player.playerTotal += (pId.W * 20);
						player.wickets = pId.W;
					}
				});
			});
		});
		this.getFieldingRecord(this.fielding);
	}

	private getFieldingRecord(catches): void {
		catches.forEach((val) => {
			val.scores.forEach((pId) => {
				this.playerRecord.forEach((player) => {
					if (+player.pid === +pId.pid) {
						if (!player.playerTotal) {
							player.playerTotal = this.playerTotal;
						}
						player.fielding = pId.runout + pId.catch + pId.stumped;
						player.playerTotal += (player.fielding * 10);
					}
				});
			});
		});
		this.getUserScore(this.playerRecord);
	}

	private getUserScore(matchPlayers): void {
		let totalScore: number = 0;
		matchPlayers.forEach((total) => {
			if (total.playerTotal) {
				totalScore += total.playerTotal;
			}
		});
		this.userScore = totalScore;
		this.addScore(this.userScore);
	}

	private addScore(score): void {
		let fantasyScore = {
			fantasyScore: score
		};
		this.squadSelectionService.addFantasyScore(this.userId, fantasyScore).subscribe(data => data);
	}

}
