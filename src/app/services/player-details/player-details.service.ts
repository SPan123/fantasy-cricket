import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class PlayerDetailsService {

  private url = 'http://cricapi.com/api/playerFinder';

  constructor(private http: HttpClient) { }

  public findPlayerDetails(query): Observable<any> {
    return this.http.get(this.url + '?apikey=q9fIAFX6FRTzSLpQEtG4EPWFTxn1&name=' + query).pipe(
      (map(response => response))
    );
  }
}
