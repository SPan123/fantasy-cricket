import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';


@Injectable()

export class UserDetailsService {

	public loginError: boolean;

	private url: string = "http://ec2-13-57-215-24.us-west-1.compute.amazonaws.com/api";


	constructor(private http: HttpClient, private route: Router) { }

	public getUserData(uName): Observable<any> {
		return this.http.get(`${this.url}/getUserInfo?uName=${uName}`).pipe(
			map((response) => response));
	}
	public addUserDetails(body: any): Observable<any> {
		return this.http.post(`${this.url}/addUserInfo`, body).pipe(
			map(response => response));
	}
	public getUserDetails(userId?: any): Observable<any> {
		if(userId) {
			userId = userId;
		}
		else {
			userId = " ";
		}
		return this.http.get(`${this.url}/getUserInfo?userId=${userId}`).pipe(
			map(response => response));
	}
	// public getmatchDetails()

	public validateUser(uName: string, pass: string, user: any): void {
		if ((user.length !== 0) && (uName === user[0].userName) && pass === user[0].password) {
			this.route.navigate(['/player-selection']);
			localStorage.setItem('userId', user[0].id);
			this.loginError = false;
		}
		else {
			this.loginError = true;
		}
	}

	public getUserScores(matchId: any): Observable<any> {
		return this.http.get(`${this.url}/getUserScores?matchId=${matchId}`).pipe(
			map(response => response)
		)
	}
}