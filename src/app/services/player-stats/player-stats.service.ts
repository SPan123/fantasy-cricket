import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class PlayerStatsService {

  private url = 'http://cricapi.com/api/playerStats';

  constructor(private http: HttpClient) { }

  public getPlayerStats(pId: number): Observable<any> {
    return this.http.get(this.url + '?apikey=q9fIAFX6FRTzSLpQEtG4EPWFTxn1&pid=' + pId).pipe(
      map(response => response)
      );
  }
}
