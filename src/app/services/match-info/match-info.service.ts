import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class MatchInfoService {

	private url = 'http://cricapi.com/api/';

	constructor(private http: HttpClient) { }

	public idSource = new BehaviorSubject(1);

	uniqueIdSource = this.idSource.asObservable();

	public getAll(str: string): Observable<any> {
		return this.http.get(`${this.url}${str}?apikey=q9fIAFX6FRTzSLpQEtG4EPWFTxn1`).pipe(
			map(response => response));
	}

	public getScores(uId: number): Observable<any> {
		return this.http.get(`${this.url}/fantasySummary?apikey=q9fIAFX6FRTzSLpQEtG4EPWFTxn1&unique_id=${uId}`).pipe(
			map(response => response));
	}

	public getFinalScore(uId: number): Observable<any> {
		return this.http.get(`${this.url}/cricketScore?apikey=q9fIAFX6FRTzSLpQEtG4EPWFTxn1&unique_id=${uId}`).pipe(
			map(response => response));
	}

	public getId(id: number): void {
		this.idSource.next(id);
	}
}
