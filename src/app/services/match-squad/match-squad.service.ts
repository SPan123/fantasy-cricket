import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class MatchSquadService {

	private url = 'http://cricapi.com/api/fantasySquad/squad';

	private dataSource = new BehaviorSubject(1);

	squadData = this.dataSource.asObservable();

	constructor(private http: HttpClient) { }

	public getAll(id: number): Observable<any> {
		return this.http.get(this.url + '?apikey=q9fIAFX6FRTzSLpQEtG4EPWFTxn1&unique_id=' + id)
			.pipe((map(response => response)));
	}

	getMatchData(uId: number) {
		this.dataSource.next(uId);
	}

	public isExsist(dataList: any[], pId: any): boolean {
		let counter: number = 0;
		if(dataList.length > 0) {
			dataList.forEach((player) => {
				if(player.playerId === pId) {
					counter++;
				}
			});
			if(counter > 0) {
				return false;
			}
			else {
				return true;
			}
		}
		else {
			return true;
		}
	}

}
