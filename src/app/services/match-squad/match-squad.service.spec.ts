import { TestBed } from '@angular/core/testing';

import { MatchSquadService } from './match-squad.service';

describe('MatchSquadService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MatchSquadService = TestBed.get(MatchSquadService);
    expect(service).toBeTruthy();
  });
});
