import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { PlayerList } from '../../model/player-list';

@Injectable()
export class SquadSelctionService {

	private url = "http://ec2-13-57-215-24.us-west-1.compute.amazonaws.com/api"

	constructor(private http: HttpClient) { }

	public addPlayers(qParam, body): Observable<any> {
		return this.http.post(`${this.url}/addMatchInfo?userId=${qParam}`, body).pipe(
			(map(response => response))
		);
	}

	public addFantasyScore(qParam, body): Observable<any> {
		return this.http.post(`${this.url}/addFantasyScore?userId=${qParam}`, body).pipe(
			(map(response => response))
		);
	}
}
