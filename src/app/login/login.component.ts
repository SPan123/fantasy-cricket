import { Component, OnInit } from '@angular/core';
import { UserDetailsService } from '../services/user-details/user-details.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import {Md5} from 'ts-md5/dist/md5';


@Component({
	selector: 'fc-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	public details: any;
	public validationError: string;
	public login: boolean = true;
	public signup: boolean = false;
	public loginErrorMsg: string;
	

	constructor(private userDetailsService: UserDetailsService,
		private formBuilder: FormBuilder
		) {
	}

	public signUpForm: FormGroup = this.formBuilder.group({
		firstName: ['', Validators.required],
		lastName: ['', Validators.required],
		signUpUserName: ['', Validators.required],
		email: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')]],
		signUpPassword: ['', Validators.required],
		confirmPassword: ['', Validators.required]
	});

	public signInForm = this.formBuilder.group({
		signInUserName: ['', Validators.required],
		signInPassword: ['', Validators.required]
	});

	public ngOnInit():void {
	}

	public collectFormData(): void {
		event.preventDefault();
		this.signUpForm.value.signUpUserName = this.signUpForm.value.signUpUserName.toLowerCase();
		this.signUpForm.value.signUpPassword = Md5.hashStr(this.signUpForm.value.signUpPassword);
		this.signUpForm.value.confirmPassword = Md5.hashStr(this.signUpForm.value.confirmPassword);
		this.signUpForm.value.email = this.signUpForm.value.email.toLowerCase();

		if (this.signUpForm.value.signUpPassword === this.signUpForm.value.confirmPassword && this.signUpForm.valid) {
			this.userDetailsService.addUserDetails(this.signUpForm.value).subscribe(data => data);
			this.signUpForm.reset();
			this.signup = false;
			this.login = true;
			this.loginErrorMsg = '';
		}
		else {
			if (this.signUpForm.value.signUpPassword !== this.signUpForm.value.confirmPassword) {
				this.validationError = 'Password must match!!';
			}
		}
	}

	public userLogin(): void {
		let userName: string;
		let password: any;
		userName = this.signInForm.value.signInUserName.toLowerCase();
		password = Md5.hashStr(this.signInForm.value.signInPassword);
		this.userDetailsService.getUserData(userName).subscribe((data) => {
			this.userDetailsService.validateUser(userName, password, data);
			if(this.userDetailsService.loginError) {
				this.loginErrorMsg = 'Username or Password is incorrect! Please try again.';
			}
		});
	}

}