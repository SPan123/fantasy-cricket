import { Component, OnInit } from '@angular/core';
import { MatchSquadService } from '../services/match-squad/match-squad.service';
import { SquadSelctionService } from '../services/squad-selction/squad-selction.service';
import { PlayerDetailsService } from '../services/player-details/player-details.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
	selector: 'fc-match-squad',
	templateUrl: './match-squad.component.html',
	styleUrls: ['./match-squad.component.scss']
})
export class MatchSquadComponent implements OnInit {

	public sqData: any;
	public playerList:any[] = [];
	public matchId: number;
	public playerDetails : any[];
	private userId: number;
	public data: any;

	constructor(private matchSquadService: MatchSquadService,
		private squadSelctionService: SquadSelctionService,
		private playerDetailsService: PlayerDetailsService,
		private route: Router,
		private activatedRoute: ActivatedRoute
		) { }

	ngOnInit() {
		this.matchSquadService.squadData.subscribe((data) => this.matchId = data);

		this.matchSquadService.getAll(this.matchId).subscribe((data) => {
			this.sqData = data;
		});
		this.userId = localStorage.userId;
	}

	public finalList() {
		let selectedSquad = {
			selectedPlayers : {
				matchId: this.matchId,
				players: this.playerList
			}
		};
		this.squadSelctionService.addPlayers(this.userId, selectedSquad).subscribe((data) => {
			if(data === 204) {
				this.route.navigate(['']);
			}
		});
	}

	public playerStats(pid): void {
		this.route.navigate(['player-info', pid]);
	}

	public selectPlayers(ev, pid: any, pName: string): void {
		let selectedPlayer: Element = ev.currentTarget as HTMLElement;
		selectedPlayer = selectedPlayer.previousElementSibling;
		const selectionLimit: number = 11;
		if (this.matchSquadService.isExsist(this.playerList, pid) && (this.playerList.length < selectionLimit)) {
			this.playerList.push({
				playerName: pName,
				playerId: pid
			});
			selectedPlayer.parentElement.classList.add('selected');
		}
	}

	public unSelectPlayers(ev, pid: any, pName: string): void {
		let unSelectedPlayer: Element = ev.currentTarget as HTMLElement;
		unSelectedPlayer = unSelectedPlayer.previousElementSibling;
		const selctedPlayers = Array.from(document.querySelectorAll('.selected'));
		selctedPlayers.forEach((val) => {
			if(val.textContent === pName){
				val.classList.remove('selected');
			}
		});
		this.playerList.forEach((player, id) => {
			if(player.playerId === pid) {
				this.playerList.splice(id, 1);
			}
		});
	}

}
