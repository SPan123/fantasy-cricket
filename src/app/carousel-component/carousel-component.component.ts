import { Component, OnInit, ElementRef, Input, AfterViewInit } from '@angular/core';
import { MatchInfoService } from '../services/match-info/match-info.service';
import { MatchSquadService } from '../services/match-squad/match-squad.service';
import { Router } from '@angular/router';
import { UserDetailsService } from '../services/user-details/user-details.service';

@Component({
    selector: 'fc-carousel-component',
    templateUrl: './carousel-component.component.html',
    styleUrls: ['./carousel-component.component.scss']
})
export class CarouselComponentComponent implements OnInit, AfterViewInit {

    @Input() matches: any;
    @Input() userId: number;
    public activeGame: boolean;
    public selectedGame: boolean;
    private matchId: number;


    constructor(private el: ElementRef,
        private route: Router,
        private matchInfoService: MatchInfoService,
        private matchSquadService: MatchSquadService,
        private userDetailsService: UserDetailsService
    ) { }

    public getFullSquad(id: number): void {
        if(this.userId) {
            this.matchSquadService.getMatchData(id);
            this.route.navigate(['/match-squad', this.userId]);
        }
        else {
            this.route.navigate(['/login']);
        }
    }

    public getUniqueId(userId: number): void {
        this.matchInfoService.getId(userId);
        this.route.navigate(['/match-details']);
    }

    public ngOnInit(): void {
        if (this.userId) {
            this.userDetailsService.getUserDetails(this.userId).subscribe(data => {
                if (data[0].matchInfo) {
                    this.matchId = data[0].matchId;
                    this.isGameActive(this.matchId);
                }
                else {
                    this.activeGame = false;
                }
            });
        }
        else {
            this.activeGame = false;
        }
    }

    private isGameActive(id: number): void {
        this.matchInfoService.getScores(id).subscribe(data => {
            if (data.data.winner_team || !data.data.matchStarted) {
                this.activeGame = false;
                this.selectedGame = false;
            }
            else {
                this.activeGame = true;
                this.selectedGame = true;
            }
        });
    }

    public ngAfterViewInit(): void {
        this.carousel(this.el.nativeElement.childNodes[0]);
    }

    public carousel(root): void {
        let
            figure = root.querySelector('figure'),
            nav = root.querySelector('nav'),
            cards = figure.children,
            n = cards.length,
            gap = root.dataset.gap || 0,
            bfc = 'bfc' in root.dataset,

            theta = 2 * Math.PI / n,
            currImage = 0
            ;

        setupCarousel(n, parseFloat(getComputedStyle(cards[0]).width));
        window.addEventListener('resize', () => {
            setupCarousel(n, parseFloat(getComputedStyle(cards[0]).width))
        });

        setupNavigation();

        function setupCarousel(n, s) {
            let apothem = s / (2 * Math.tan(Math.PI / n));
            let i: number;

            figure.style.transformOrigin = `50% 50% ${- apothem}px`;

            for (i = 0; i < n; i++)
                cards[i].style.padding = `${gap}px`;
            for (i = 1; i < n; i++) {
                cards[i].style.transformOrigin = `50% 50% ${- apothem}px`;
                cards[i].style.transform = `rotateY(${i * theta}rad)`;
            }
            if (bfc)
                for (i = 0; i < n; i++)
                    cards[i].style.backfaceVisibility = 'hidden';

            rotateCarousel(currImage);
        }

        function setupNavigation() {
            nav.addEventListener('click', onClick, true);

            function onClick(e) {

                let t = <HTMLElement>e.target;

                if (t.tagName.toUpperCase() != 'A' && t.tagName.toUpperCase() != 'I') {
                    return;
                }

                if (t.classList.contains('next') || t.parentElement.classList.contains('next')) {
                    currImage++;
                }
                else {
                    currImage--;
                }

                rotateCarousel(currImage);
            }

        }

        function rotateCarousel(imageIndex) {
            figure.style.transform = `rotateY(${imageIndex * -theta}rad)`;
        }

    }

    public ngOnDestroy(): void {
    }

}
