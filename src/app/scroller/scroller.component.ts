import { Component, OnInit, HostListener, Input } from '@angular/core';

@Component({
	selector: 'fc-scroller',
	templateUrl: './scroller.component.html',
	styleUrls: ['./scroller.component.scss']
})
export class ScrollerComponent implements OnInit {

	@Input() hideScroller: boolean;

	constructor() { }

	public ngOnInit(): void {
	}

	public scrolledHeight: number = window.innerHeight;
	public scrollDown(): void {
		window.scrollTo({
			top: this.scrolledHeight,
			behavior: "smooth"
		});
	}

	@HostListener('window:scroll', [])
	public onWindowScroll(): void {
		this.scrolledHeight = window.scrollY + window.innerHeight;
	}

}
